const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	
quantity : {
	type : Number,
	required : [true, 'Quantity is required!']
},

totalAmount : {
	type : Number,
	default : 0
},
createdOn: {
	type: Date,
	default: new Date()
},
productId: {
	type: String,
	ref: 'Product'
},
userId: {
	type: mongoose.Schema.Types.ObjectId,
	ref:'User',
}

});

module.exports = mongoose.model('Order', orderSchema);




