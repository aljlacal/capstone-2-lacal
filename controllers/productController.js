const User = require('../models/userModel');
const Order = require('../models/orderModel');
const Product = require('../models/productModel');
const auth = require('../auth');
const bcrypt = require('bcrypt');




module.exports.addProduct = (req, res) => {

	Product.findOne({productName: req.body.productName}).then(result => {
		if(result != null){
			res.send({
				message: "Product is already exists!"
			});
		}else {

			let productName = req.body.productName;
			let description = req.body.description;
			let price = req.body.price;

			let newProduct = new Product({
				productName: productName,
				description: description,
				price: price
			});

			newProduct.save().then(product => {
				res.send(product)
			})
			.catch(err => {
				res.send(err)
			});
		}
	});
}



module.exports.retrieveAllProduct = (req, res) =>{
	
	Product.find({}).then((result) => {
		res.send({data: result})
	}).catch(err => {
		return err;
	});
}


module.exports.retrieveSpecificProduct = (req, res) => {

	let productId = req.params.productId;

	Product.find({_id: productId}).then(foundProduct =>{
		if(foundProduct.length > 0)
		res.send({product: foundProduct});
	}).catch(err =>{
		res.send(err);
	});
}


module.exports.updateProduct = (req, res) => {

	let productId = req.params.productId;
	let updates = {$set: req.body};

	let options = {new: true}

	Product.findByIdAndUpdate(productId, updates, options).then(result => {
		res.send({
			updated: result
		});
	}).catch(err =>{
		res.send(err)
	});
}


module.exports.archiveProduct = (req, res) => {

	let productId = req.params.productId;

	Product.findOne({_id: productId}).then(result =>{
		if(result.isActive){

			result.isActive = false;

			result.save().then(archivedProduct => {
				res.send({
					message: "Product archived successfully!"
				});
			}).catch(err =>{
				res.send(err)
			});
		} else {
			res.send({
				message: "Product is already archived!"
			});
		}
	}).catch(err => {
		res.send(err)
	});
}


module.exports.activeProduct = (req, res) => {

	let productId = req.params.productId;

	Product.findOne({_id: productId}).then(result =>{
		if(result.isActive === false){

			result.isActive = true;

			result.save().then(activeProduct => {
				res.send({
					message: "Product activate successfully!"
				});
			}).catch(err =>{
				res.send(err)
			});
		} else {
			res.send({
				message: "Product is already activate!"
			});
		}
	}).catch(err => {
		res.send(err)
	});
}





