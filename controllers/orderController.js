const User = require('../models/userModel');
const Order = require('../models/orderModel');
const Product = require('../models/productModel');
const auth = require('../auth');


module.exports.createOrder = (req, res) => {

	let userId = req.user.id;
	let productId = req.body.productId;
	let quantity = req.body.quantity;


	 Product.findOne({ _id: productId }).then(product => {

			let price = product.price;
			let totalAmount = price * quantity;

			let newOrder = new Order({
			productId: productId,
			quantity: quantity,
			userId: userId,
			totalAmount: totalAmount
			});

		 newOrder.save().then(result => {
		 	res.send({
		 		message: "Order successful!", 
		 		data: result
		 	});

		}).catch(err => {
       		res.send(err)
  		});
	

	}).catch(err => {
       	res.send(err)
  	});
}



module.exports.usersOrder = (req,res) => {

	let userId  = req.user.id;
	Order.find({ userId : userId})
	// ({userId: req.user.id})

	.then ((result) => {
			res.send({order: result})
	})
	.catch( err => {
		res.send(err)
	})
}



module.exports.getAllOrder = (req,res) => {
	// res.send("All orders route")

	Order.find()
	.then(Users => {
		res.send(Users)
	})
	.catch(err => {
		res.send(err)
	})
}


// module.exports.usersOrder = (request) => {

// return Order.find({userId: request.user.id}).then((result)=>{
// 	if(result){
// 		return {data: result}
// 	}else{
// 		return {message: "No data found!"}
// 		}
// 	}).catch(err => {
// 		return err;
// 	});
// }




// module.exports.usersOrder = (req, res) => {

// order.find({userId: {$in: req.params.userId}}).then(order =>{
// 	res.send(order)
// }).catch(err =>{
// 	res.send(err)
// })
// }






