const User = require('../models/userModel');
const Order = require('../models/orderModel');
const Product = require('../models/productModel');
const auth = require('../auth');

const bcrypt = require('bcrypt');


// function to register

module.exports.register = (request, response) => {
	
User.findOne({email: request.body.email})
.then(result => {
	if(result != null){
		response.send({
			message: "User email is already taken!"
		});
	} else {

		let firstName = request.body.firstName;
		let lastName = request.body.lastName;
		let email = request.body.email;
		let password = request.body.password;
		let mobileNo = request.body.mobileNo;
		let address = request.body.address;
	

		if(
			(firstName != '' && lastName != '' && email != '' && password != '' && mobileNo != '' && address != '') &&
			(firstName != null && lastName != null && email != null && password != null && mobileNo != null && address!= null) &&
			(firstName != undefined && lastName != undefined && email != undefined && password != undefined && mobileNo != undefined && address != undefined)
			){
			const hashedPassword = bcrypt.hashSync(request.body.password, 10);
		
			let newUser = new User({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: hashedPassword,
				mobileNo: mobileNo,
				address: address
			});
				
				newUser.save()
				.then(user => {
					response.send(user)
				})
				.catch(err => {
					response.send(err)
				});
			} else {
				response.send({
					message: "All fields are required!"
				});
			}
		}
	}).catch(err => {
		res.send(err)
	});

}


// function to login

module.exports.login = (req, res) => {
	
	let email = req.body.email;
	let password = req.body.password;

	User.findOne({email:email}).then(foundUser => {
		if(foundUser == null){ 
			res.send({
				message: "User Not Found!"
			})
		} else {
		
		const isPasswordCorrect = bcrypt.compareSync(password, foundUser.password);
		if(isPasswordCorrect){ 
			res.send({ accessToken: auth.createAccessToken(foundUser)});
		} else {
			res.send({
				message: "Incorrect password!"
			})
		}
	}
	}).catch(err => {
		return err;
	})
}


// function to get user detail

module.exports.getUserDetails = (req, res) => {

	let userId = req.user.id;
	User.findById(userId)
	.then(foundUser => {
		res.send(foundUser);
	})
	.catch(err => {
		res.send(err);
	});
}


// function to update acct

module.exports.updateDetails = (req, res) => {
	let userId = req.user.id;
	let updates = {$set: req.body};
		// firstName: req.body.firstName,
		// lastName: req.body.lastName,
		// mobileNo: req.body.mobileNo,
		// address: req.body.address

	let options = {new:true}

	User.findByIdAndUpdate(userId, updates, options).then(result => {
		res.send({
			updated: result
		});
	}).catch(err => {
		// res.send(err);
	});
}



// function to make user to admin

module.exports.userToAdmin = (req, res) => {

	let userId = req.user.id;
	let makeAdmin = req.body.userId;
	// let isAdmin = req.body.isAdmin;

	User.findOne({_id: makeAdmin}).then(result =>{
		if(result.isAdmin === false){
			result.isAdmin = true;

			result.save().then(userToAdmin => {
				res.send({
					message: "You are admin now!", newAdmin: result
				})
			}).catch(err =>{
				res.send(err)
			});
		} else {
			res.send({
				message: "You are already set as an admin!!"
			});
		}
	}).catch(err => {
		res.send(err)
	});
}
