const express = require('express');
const router = express.Router();

const productController = require('../controllers/productController');
const auth = require('../auth');


router.post('/product', auth.verify, auth.verifyIsAdmin, productController.addProduct);


router.get('/products', productController.retrieveAllProduct);

router.get('/products/:productId', productController.retrieveSpecificProduct);

router.patch('/update/product/:productId', auth.verify, auth.verifyIsAdmin, productController.updateProduct);

router.put('/archive/product/:productId', auth.verify, auth.verifyIsAdmin, productController.archiveProduct);

router.put('/active/product/:productId',auth.verify, auth.verifyIsAdmin, productController.activeProduct)

module.exports = router
