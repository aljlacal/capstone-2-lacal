const express = require('express');
const router = express.Router();

const orderController = require('../controllers/orderController');

const auth = require('../auth');



router.post('/order', auth.verify, auth.verifyIsOrdinaryUser, orderController.createOrder);


router.get('/user/order', auth.verify, auth.verifyIsOrdinaryUser, orderController.usersOrder);

router.get('/myOrder', auth.verify, auth.verifyIsAdmin, orderController.getAllOrder);


module.exports = router