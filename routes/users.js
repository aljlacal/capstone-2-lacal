const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

router.post('/register', userController.register);

router.post('/login', userController.login);

router.get('/profile', auth.verify, userController.getUserDetails);

router.patch('/profile/update', auth.verify, userController.updateDetails);

router.put('/admin', auth.verify, auth.verifyIsAdmin, userController.userToAdmin);



module.exports = router