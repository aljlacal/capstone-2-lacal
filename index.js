const express = require('express');
const mongoose = require('mongoose');
const users = require('./routes/users');
const products = require('./routes/products');
const orders = require('./routes/orders');
require('dotenv').config();
const cors = require('cors');



const app = express();
const port =process.env.PORT;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect(process.env.DB_MONGODB_ATLAS, 
{useNewUrlParser: true, useUnifiedTopology: true});
let db = mongoose.connection;

db.on('error', console.error.bind(console, "Connection error!"));
db.on('open', ()=> console.log("We are connected to the Aljohn Store!"));




app.get('/', (req, res) => {
	res.send("Welcome to My Capstone Project 2!")
});

app.use('/api', users)

app.use('/api', products)

app.use('/api', orders)


app.listen(port, () => console.log(`server is listening to port ${port}`));